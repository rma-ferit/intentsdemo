package brunozoric.ferit.hr.intentsdemo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_details.*
import kotlinx.android.synthetic.main.activity_user.*

class UserActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user)
        setUpUi()
    }

    private fun setUpUi() {
        detailsNavigationAction.setOnClickListener{ navigateToDetails()}
    }

    private fun navigateToDetails() {
        val detailsIntent: Intent = Intent(this, DetailsActivity::class.java)
        startActivity(detailsIntent)
    }
}
